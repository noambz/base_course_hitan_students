package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ejectedPilotRescue")
public class EjectedPilotRescueController {
    private EjectedPilotRescueProvider ejectedPilotRescueProvider;
    private AirplanesAllocationManager airplanesAllocationManager;

    public EjectedPilotRescueController( @Autowired EjectedPilotRescueProvider ejectedPilotRescueProvider,
                                         @Autowired AirplanesAllocationManager airplanesAllocationManager) {
        this.ejectedPilotRescueProvider = ejectedPilotRescueProvider;
        this.airplanesAllocationManager = airplanesAllocationManager;
    }

    @GetMapping("/infos")
    public List<EjectedPilotInfo> getEjectedPilotInfos(){
        return ejectedPilotRescueProvider.getEjectedPilotInfos();
    }

    @GetMapping("/takeResponsibility")
    public void takeResponsibility(
            @RequestParam int ejectionId,
            @CookieValue("client-id") String clientId){
        EjectedPilotInfo pilot = ejectedPilotRescueProvider.getEjectedPilotInfoById(ejectionId);
        pilot.setRescuedBy(clientId);
        ejectedPilotRescueProvider.updateEjectedPilotInfo(pilot);
        airplanesAllocationManager.allocateAirplanesForEjection(pilot, clientId);
    }
}
