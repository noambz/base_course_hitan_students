package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import java.util.List;

public interface EjectedPilotRescueProvider {
    List<EjectedPilotInfo> getEjectedPilotInfos();
    EjectedPilotInfo getEjectedPilotInfoById(int id);
    void updateEjectedPilotInfo(EjectedPilotInfo ejectedPilotInfo);
}
